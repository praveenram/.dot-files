#!/bin/bash

function git_parse_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ [\1]/'
}

function production_change_log() {
  git log --oneline --format='%s' current-production..HEAD
}

function contrib-history() {
  git log --shortstat --pretty="%cE" --since="2017-08-14T00:00:00-00:00"| sed 's/\(.*\)@.*/\1/'| grep -v "^$"| awk 'BEGIN { line=""; } !/^ / { if (line=="" || !match(line, $0)) {line = $0 "," line }} /^ / { print line " # " $0; line=""}'| sort | sed -E 's/# //;s/ files? changed,//;s/([0-9]+) ([0-9]+ deletion)/\1 0 insertions\(+\), \2/;s/\(\+\)$/\(\+\), 0 deletions\(-\)/;s/insertions?\(\+\), //;s/ deletions?\(-\)//'| awk 'BEGIN {name=""; files=0; insertions=0; deletions=0;} {if ($1 != name && name != "") { print name ": " files " files changed, " insertions " insertions(+), " deletions " deletions(-), " insertions-deletions " net"; files=0; insertions=0; deletions=0; name=$1; } name=$1; files+=$2; insertions+=$3; deletions+=$4} END {print name ": " files " files changed, " insertions " insertions(+), " deletions " deletions(-), " insertions-deletions " net";}'
}

function contrib-history-all-time() {
  git log --shortstat --pretty="%cE" | sed 's/\(.*\)@.*/\1/'| grep -v "^$"| awk 'BEGIN { line=""; } !/^ / { if (line=="" || !match(line, $0)) {line = $0 "," line }} /^ / { print line " # " $0; line=""}'| sort | sed -E 's/# //;s/ files? changed,//;s/([0-9]+) ([0-9]+ deletion)/\1 0 insertions\(+\), \2/;s/\(\+\)$/\(\+\), 0 deletions\(-\)/;s/insertions?\(\+\), //;s/ deletions?\(-\)//'| awk 'BEGIN {name=""; files=0; insertions=0; deletions=0;} {if ($1 != name && name != "") { print name ": " files " files changed, " insertions " insertions(+), " deletions " deletions(-), " insertions-deletions " net"; files=0; insertions=0; deletions=0; name=$1; } name=$1; files+=$2; insertions+=$3; deletions+=$4} END {print name ": " files " files changed, " insertions " insertions(+), " deletions " deletions(-), " insertions-deletions " net";}'
}

function fetch-latest-remote() {
  status=$(git status -s)
  curr_branch=$(git rev-parse --abbrev-ref HEAD)
  if [[ $status == '' ]]; then
    echo -e "${YELLOW} Updating branch: $curr_branch ${RESET}"
    git pull
  else
    echo -e "${RED} Cannot update branch: $curr_branch, found diff: ${RESET}"
    echo -e "$status ${RESET}"
  fi
}

function update-projects() {
  curr_dir=$(pwd)
  cd $PROJECTS_DIR
  for proj in $(find . -maxdepth 1 -type d); do
    cd $proj
    if [[ -d .git ]]; then
      echo -e "${YELLOW} Update project $proj ${RESET}"
      curr_branch=$(git rev-parse --abbrev-ref HEAD)
      fetch-latest-remote
      if [[ $curr_branch != 'master' ]]; then
        echo -e "${YELLOW} Updating branch master ${RESET}"
  git switch master
  fetch-latest-remote
  git switch -
      fi
    fi
    unset $curr_branch
    cd - > /dev/null
  done
  cd $curr_dir
}

function update-external-scripts() {
  curr_dir=$(pwd)
  cd ~/.external_scripts/repos
  for proj in $(find . -maxdepth 1 -type d); do
    cd $proj
    if [[ -d .git ]]; then
      status=$(git status -s)
      if [[ $status == '' ]]; then
        echo -e "${YELLOW} Update project $proj ${RESET}"
        git pull
      fi
    fi
    cd - > /dev/null
  done
  cd $curr_dir
}
